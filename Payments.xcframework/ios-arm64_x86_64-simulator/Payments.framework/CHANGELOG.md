# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## Index
- [Version 16](#version-16)

## [Unreleased]

## [16.0.22] - 2025-02-13

- [DIN-891](https://geopagos.atlassian.net/browse/DIN-891) `.cmdTimeout` error and `.timeout` thrown by the reader are ignored when the action to be executed is `.disconnecting`.
- [MBTX-3369](https://geopagos.atlassian.net/browse/MBTX-3369) `cvmResult` is validated for the case of `noCvm` where it returns `notAuthenticated`.

## Version 16

## [16.0.18] - 2025-01-23
- [MBTX-3558](https://geopagos.atlassian.net/browse/MBTX-3558) Se corrige memory leaks relacionado al registro de un nuevo dispositivo, en `onRegisteredDevice`.
- [MBTX-3558](https://geopagos.atlassian.net/browse/MBTX-3558) Se agrega tests para validar posibles memory leaks en `DeviceScanService`.

## [16.0.18] - 2025-01-03

### Fixed

- [MBTX-3385](https://geopagos.atlassian.net/browse/MBTX-3385) Se corrige validacion del readerType en los lectores QPos.
- [MBTX-3489](https://geopagos.atlassian.net/browse/MBTX-3489) Se corrige error en el SDK que no permitia abortar la transaction luego de haber obtenido un error .emvDeclined del lector.
- [MBTX-3414](https://geopagos.atlassian.net/browse/MBTX-3414) Se corrige error en el SDK al intentar actualizar el firmware del lector al estar desconectado de la corriente. Se hace una nueva peticion al readerInfo del lector uan vez ejectuado el retry.
- [MBTX-3492](https://geopagos.atlassian.net/browse/MBTX-3492) Se settea estado abortable al obtener un error error desde el lector.
- [MBTX-3490](https://geopagos.atlassian.net/browse/MBTX-3490) Se settea estado abortable al obtener error cuando se envia la confirmacion.
- [MBTX-3351](https://geopagos.atlassian.net/browse/MBTX-3351) Se elimina BITCODE del proyecto, ya que este se requeria para iXguard pero fue deprecado.

## [16.0.11] - 2024-11-15

### Fixed
- [MBTX-2836](https://geopagos.atlassian.net/browse/MBTX-2836) Se corrige error que impedia al SDK desconectarse del lector cuando la transaccion era abortada exitosamente.

## [16.0.9] - 2024-06-28

- [MRS-502](https://geopagos.atlassian.net/browse/MRS-502) Se agrega case HardwareCancelled al enum TransactionResult. Ahora al cencelar una transaccion en el state WaitingForCard, el SDK devuelve un error recuperable TransactionResult.Reason.hardwareCancelled.

## [16.0.7] - 2024-04-09
- [MBTX-2806](https://geopagos.atlassian.net/browse/MBTX-2806) Fix en el caso de intentar abortar una transaccion con respuesta "Terminated". Ahora dicho caso se contempla como un abort exitoso

## [16.0.6] - 2024-04-03
### Added
- [MBTX-1360](https://geopagos.atlassian.net/browse/MBTX-1360) Agregamos archivo CHANGELOG.md al versionado del proyecto para llevar detalles tecnicos de lo implementado en cada version

### Fixed
- [MBTX-1360](https://geopagos.atlassian.net/browse/MBTX-2791) Fix en prevencion de transacciones simultaneas, corrigiendo y garantizando el orden en que llegan los estados al crear una segunda transaccion mientras la primera estaba en curso

- [MBTX-2806](https://geopagos.atlassian.net/browse/MBTX-2806) Fix en el caso de intentar abortar una transaccion con respuesta "Terminated". Ahora dicho caso se contempla como un abort exitoso
