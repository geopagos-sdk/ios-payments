// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.10 (swiftlang-5.10.0.13 clang-1500.3.9.4)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name Payments
import Foundation
@_exported import Payments
import Swift
import TransactionSDK
import Transactionsdk_plugins_interfaces
import URLSessionHTTPClient
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public typealias ConfigurationRequiredCallback = (any TransactionSDK.ReaderConfigurationFiles) -> Swift.Void
public protocol ReaderConfiguratorDelegate : AnyObject {
  func onReaderStateChanged(state: Payments.ReaderState)
  func onDeviceRequired(provideDevice: @escaping Payments.DeviceRequiredCallback)
  func onConfigurationRequired(configuration: @escaping Payments.ConfigurationRequiredCallback)
  func onConfigurationCompleted()
  func onRecoverableError(error: Payments.ReaderConfiguratorError, recover: @escaping Payments.RejectRecoverCallback)
  func onNonRecoverableError(error: Payments.ReaderConfiguratorError, message: Swift.String?)
}
final public class DeviceConfigurationBuilder {
  public init()
  final public func setTerminalCapabilities(_ value: Swift.String) -> Payments.DeviceConfigurationBuilder
  final public func setCountryCode(_ value: Swift.String) -> Payments.DeviceConfigurationBuilder
  final public func setContactCvmLimit(_ value: Payments.Money) -> Payments.DeviceConfigurationBuilder
  final public func setContactlessCvmLimit(_ value: Payments.Money) -> Payments.DeviceConfigurationBuilder
  final public func build() -> TransactionSDK.DeviceConfiguration
  @objc deinit
}
public protocol Device : TransactionSDK.Device {
}
extension TransactionSDK.NormalDevice : Payments.Device {
}
public enum DeviceType {
  case qpos
  case magicPos
  case unknown
  public static func == (a: Payments.DeviceType, b: Payments.DeviceType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct ReaderInfo {
  public let id: Swift.String
  public let serial: Swift.String
  public let batteryInfo: Payments.BatteryInfo?
  public let firmware: Swift.String?
  public let hardwareVersion: Swift.String?
  public let readerType: Payments.ReaderType
  public init(id: Swift.String, serial: Swift.String, batteryInfo: Payments.BatteryInfo, firmware: Swift.String?, hardwareVersion: Swift.String?, readerType: Payments.ReaderType)
}
public struct BatteryInfo {
  public init(percentage: Swift.Double, charging: Swift.Bool)
}
public enum ReaderType {
  case qposMini
  case qposCute
  case qposBt
  case magicPos
  public static func == (a: Payments.ReaderType, b: Payments.ReaderType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias DeviceResultCallback = (Payments.DeviceResult) -> Swift.Void
final public class DeviceScanService {
  public init(deviceType: Payments.DeviceType, deviceMappers: [any TransactionSDK.DeviceMapper])
  public init(additionalMappers: [any TransactionSDK.DeviceMapper]? = nil)
  final public func scan(timeout: Foundation.TimeInterval = 10.0, callback: @escaping Payments.DeviceResultCallback)
  final public func stopScanning()
  @objc deinit
}
@frozen public enum DeviceResult {
  case success(list: [any Payments.Device], isScanning: Swift.Bool)
  case error(type: Payments.DeviceResultErrorType, isScanning: Swift.Bool)
}
public enum DeviceResultErrorType {
  case permissionError
  case nothingFound
  case hardwareError
  public static func == (a: Payments.DeviceResultErrorType, b: Payments.DeviceResultErrorType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct PaymentsSDK {
  public static func configure(paymentsConfiguration: Payments.PaymentsConfiguration)
}
public struct QposMiniFirmwareRestoreDevice : TransactionSDK.DeviceRestore {
  public var id: ObjectiveC.NSObject {
    get
  }
  public var name: Swift.String {
    get
  }
  public var type: TransactionSDK.DeviceType {
    get
  }
  public var scanData: TransactionSDK.ScanData {
    get
  }
  public var readerInfo: TransactionSDK.ReaderInfo {
    get
  }
  public init(device: any Payments.Device, readerInfo: Payments.ReaderInfo)
}
extension Payments.QposMiniFirmwareRestoreDevice : Payments.Device {
}
public struct ReadConfig {
  public let readModes: Swift.Set<TransactionSDK.CardReadMode>
  public let timeout: Foundation.TimeInterval
  public let cardInsertionStatus: TransactionSDK.CardInsertionStatus
  public let transactionTotal: Payments.TransactionTotal
  public init(readModes: Swift.Set<TransactionSDK.CardReadMode>, timeout: Foundation.TimeInterval, cardInsertionStatus: TransactionSDK.CardInsertionStatus = .cardNotInserted, transactionTotal: Payments.TransactionTotal)
}
public protocol SDKTransactionListener {
  func onTransactionStateChanged(state: Payments.SDKTransactionState)
  func onReaderStateChanged(state: Payments.ReaderState)
}
public enum ReaderState {
  case notConnected
  case connecting(any Payments.Device)
  case connected(any Payments.Device, TransactionSDK.ReaderInfo, Payments.ReaderState.Connected)
  public enum Connected : Swift.Equatable {
    case idle
    case waitingForCard
    case processing(TransactionSDK.CardReadMode)
    case emvRequestingPin
    public static func == (a: Payments.ReaderState.Connected, b: Payments.ReaderState.Connected) -> Swift.Bool
  }
}
extension Payments.ReaderState : Swift.Equatable {
  public static func == (lhs: Payments.ReaderState, rhs: Payments.ReaderState) -> Swift.Bool
}
public enum SDKTransactionState {
  case provideDevice(provideDeviceCallback: Payments.DeviceRequiredCallback)
  case provideReadConfig(provideReadConfigCallback: Payments.ConfigRequiredCallback)
  case selectEMVApp(availableAIDs: [Payments.EmvApp], selectEMVAppCallback: Payments.EMVAppRequiredCallback)
  case confirmPayment(card: Payments.TransactionCard, confirmCallback: Payments.ConfirmPaymentDataCallback, rejectCallback: Payments.RejectRecoverCallback)
  case confirmRefund(card: Payments.TransactionCard, confirmCallback: Payments.ConfirmRefundDataCallback, rejectCallback: Payments.RejectRecoverCallback)
  case confirmCancel(card: Payments.TransactionCard, confirmCallback: Payments.ConfirmCancelDataCallback, rejectCallback: Payments.RejectRecoverCallback)
  case approved(transactionApproved: Payments.TransactionApproved)
  case recoverableError(error: Payments.TransactionIntentError, recoverCallback: Payments.RejectRecoverCallback)
  case nonRecoverableError(error: Payments.TransactionIntentError, message: Swift.String?)
}
public enum TransactionCard {
  case magnetic(Payments.MagneticCard)
  case emv(Payments.EmvCard)
  public init(card: TransactionSDK.TransactionCard) throws
}
public class Card {
  final public let maskedPan: Swift.String
  final public let cardHolderName: Swift.String?
  final public let readMode: TransactionSDK.CardReadMode
  public init(maskedPan: Swift.String, cardHolderName: Swift.String?, readMode: TransactionSDK.CardReadMode)
  @objc deinit
}
final public class MagneticCard : Payments.Card {
  final public let serviceCode: Swift.String?
  final public let needPin: Swift.Bool
  final public let hasChip: Swift.Bool
  public init(card: TransactionSDK.TransactionCard)
  @objc deinit
}
final public class EmvCard : Payments.Card {
  final public let emvApp: Payments.EmvApp
  final public let cvmResult: Payments.CvmResult
  public init(card: TransactionSDK.TransactionCard) throws
  @objc deinit
}
public enum CvmResult {
  case noCvm, signature, pin
  public init(cvmResult: TransactionSDK.CvmResult)
  public static func == (a: Payments.CvmResult, b: Payments.CvmResult) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers final public class ReaderConfiguratorFactory {
  public static func createConfigurator(delegate: any Payments.ReaderConfiguratorDelegate, additionalMappers: [any TransactionSDK.DeviceMapper]? = nil) throws -> Payments.ReaderConfigurator
  @objc deinit
}
extension URLSessionHTTPClient.GeoURLSessionHTTPClientLoggerDecorator : TransactionSDK.HTTPSerializerClient {
  final public func post<RequestType, ResponseType>(url: Foundation.URL, headers: [TransactionSDK.HTTPHeader], parameters: RequestType, completion: @escaping TransactionSDK.HTTPClientCompletionHandler<ResponseType>) where RequestType : Swift.Encodable, ResponseType : Swift.Decodable
  final public func get<ResponseType>(url: Foundation.URL, headers: [TransactionSDK.HTTPHeader], completion: @escaping TransactionSDK.HTTPClientCompletionHandler<ResponseType>) where ResponseType : Swift.Decodable
}
public struct HTTPData : Swift.Decodable {
  public let statusCode: Swift.Int
  public let data: Foundation.Data
  public init(statusCode: Swift.Int, data: Foundation.Data)
  public init(from decoder: any Swift.Decoder) throws
}
public enum ReaderConfiguratorError : Swift.Equatable {
  case invalidDevice
  case connectionFailed
  case invalidConfiguration(Swift.String?)
  case configurationError(Swift.String?)
  case readerShouldBeConnectedIntoPower(Swift.String?)
  case internalError
  case aborted
  public static func == (a: Payments.ReaderConfiguratorError, b: Payments.ReaderConfiguratorError) -> Swift.Bool
}
public enum ReaderConfiguratorInitializationError : Swift.Error {
  case missingInitialization(Swift.String)
}
public protocol Abortable {
  func abort() throws
}
public enum TransactionAbortError : Swift.Error {
  case unavailable(message: Swift.String)
}
@_hasMissingDesignatedInitializers final public class TransactionIntent {
  @objc deinit
}
extension Payments.TransactionIntent : Payments.Abortable {
  final public func abort() throws
}
@_hasMissingDesignatedInitializers final public class DeviceConfigurator {
  public static func deviceWithConfiguration(device: any Payments.Device, deviceConfiguration: TransactionSDK.DeviceConfiguration) throws -> any Payments.Device
  @objc deinit
}
public enum DeviceConfiguratorError : Swift.Error {
  case deviceNotAllow(Swift.String?)
}
public struct Confirmation {
  public let authToken: Swift.String
  public let applicationKey: Swift.String
  public let isFallback: Swift.Bool
  public let terminalId: Swift.String
  public let merchant: Payments.Merchant
  public let establishment: Payments.Establishment
  public let additionalCardData: Payments.AdditionalCardData
  public let refNumber: Swift.String
  public let timeZone: Foundation.TimeZone
  public let ticketNumber: Swift.String?
  public let accountType: Payments.AccountType?
  public let externalData: Swift.String?
  public init(authToken: Swift.String, applicationKey: Swift.String, isFallback: Swift.Bool, terminalId: Swift.String, merchant: Payments.Merchant, establishment: Payments.Establishment, additionalCardData: Payments.AdditionalCardData, refNumber: Swift.String, timeZone: Foundation.TimeZone, ticketNumber: Swift.String? = nil, accountType: Payments.AccountType? = nil, externalData: Swift.String? = nil)
}
public struct PaymentConfirmation {
  public init(confirmation: Payments.Confirmation, installments: Swift.Int?, paymentPlan: Swift.String?, updatedAmount: Payments.TransactionTotal? = nil)
}
public struct RefundConfirmation {
  public let confirmation: Payments.Confirmation
  public let parentRefNumber: Swift.String
  public init(confirmation: Payments.Confirmation, parentRefNumber: Swift.String)
}
public struct CancelConfirmation {
  public let confirmation: Payments.Confirmation
  public let parentRefNumber: Swift.String
  public init(confirmation: Payments.Confirmation, parentRefNumber: Swift.String)
}
public enum TransactionApproved {
  case sale(Payments.TransactionCard, Payments.PaymentConfirmation, Payments.PaymentData)
  case refund(Payments.TransactionCard, Payments.RefundConfirmation, Payments.PaymentData)
  case cancel(Payments.TransactionCard, Payments.CancelConfirmation, Payments.PaymentData)
}
public struct PaymentData {
  public let authCode: Swift.String?
  public init(authCode: Swift.String?)
}
public struct Merchant {
  public let merchantId: Swift.String
  public let merchantName: Swift.String?
  public let fiid: Swift.String?
  public let logicalNetwork: Swift.String?
  public init(merchantId: Swift.String, merchantName: Swift.String? = nil, fiid: Swift.String? = nil, logicalNetwork: Swift.String? = nil)
}
public struct AdditionalCardData {
  public let brand: Swift.String
  public let cardType: Payments.CardType
  public init(cvv: Swift.String?, brand: Swift.String, cardType: Payments.CardType)
}
public class Establishment {
  public init(address: Payments.Address? = nil, businessCategoryCode: Swift.String? = nil, docNumber: Swift.String? = nil, docType: Swift.String? = nil, email: Swift.String? = nil, legalName: Swift.String, phoneNumber: Swift.String? = nil, tradeName: Swift.String? = nil, establishmentType: Payments.EstablishmentType, intermediary: Payments.Establishment? = nil)
  @objc deinit
}
public struct Address {
  public let addressLine1: Swift.String?
  public let addressLine2: Swift.String?
  public let city: Swift.String
  public let countryCode: Swift.String
  public let postalCode: Swift.String
  public let state: Swift.String
  public init(addressLine1: Swift.String? = nil, addressLine2: Swift.String? = nil, city: Swift.String, countryCode: Swift.String, postalCode: Swift.String, state: Swift.String)
}
public enum EstablishmentType : Swift.String {
  case endMerchant
  case intermediary
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct Money : Swift.Equatable {
  public let amount: Foundation.Decimal
  public let currency: Payments.Currency
  public init(amount: Foundation.Decimal, currency: Payments.Currency) throws
  public static func == (a: Payments.Money, b: Payments.Money) -> Swift.Bool
}
public enum MoneyError : Swift.Error {
  case moneyInvalid(Swift.String)
  case currencyNotEqual
}
public struct Currency {
  public let alphabeticCode: Swift.String
  public static let ars: Payments.Currency
  public static let usd: Payments.Currency
  public static let mxn: Payments.Currency
  public static let cop: Payments.Currency
  public init(numericCode: Swift.String, alphabeticCode: Swift.String, exponent: Payments.Exponent = .two)
}
extension Payments.Currency : Swift.Equatable {
  public static func == (left: Payments.Currency, right: Payments.Currency) -> Swift.Bool
}
public enum Exponent {
  case zero
  case two
  public static func == (a: Payments.Exponent, b: Payments.Exponent) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Tax {
  public let amount: Payments.Money
  public init(amount: Payments.Money, label: Swift.String)
}
public protocol Taxes {
  var total: Payments.Money { get }
}
public struct BreakDownTaxes : Payments.Taxes {
  public let taxes: [Payments.Tax]
  public var total: Payments.Money {
    get
  }
  public init(taxes: [Payments.Tax]) throws
}
public struct TotalTaxes : Payments.Taxes {
  public var total: Payments.Money {
    get
  }
  public init(taxes: Payments.Money)
}
public struct TransactionTotal {
  public let net: Payments.Money
  public init(net: Payments.Money, taxes: any Payments.Taxes) throws
  public init(gross: Payments.Money)
}
extension TransactionSDK.Exponent {
  public init(exponent: Payments.Exponent)
}
@frozen public enum CardType {
  case credit
  case debit
  public static func == (a: Payments.CardType, b: Payments.CardType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum AccountType {
  case savingAccount
  case transactionAccount
  public static func == (a: Payments.AccountType, b: Payments.AccountType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol SDKLogger {
  func log(message: Swift.String, level: Payments.LogLevel)
}
public enum LogLevel {
  case debug, info, notice, error, fault
  public static func == (a: Payments.LogLevel, b: Payments.LogLevel) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers final public class PaymentsConfiguration {
  public class Builder {
    public init(endpoint: Foundation.URL, readerInstallers: [any TransactionSDK.ReaderInstaller]) throws
    public func setSwipePinDecider(swipePinDecider: any Payments.SwipePinDecider) -> Payments.PaymentsConfiguration.Builder
    public func setLogger(logger: any Payments.SDKLogger) -> Payments.PaymentsConfiguration.Builder
    public func set(plugins: [Swift.AnyObject]) -> Payments.PaymentsConfiguration.Builder
    public func build() -> Payments.PaymentsConfiguration
    @objc deinit
  }
  @objc deinit
}
public enum PaymentsConfigurationError : Swift.Error {
  case noInstallersProvided
  public static func == (a: Payments.PaymentsConfigurationError, b: Payments.PaymentsConfigurationError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias DeviceRequiredCallback = (any Payments.Device) -> Swift.Void
public typealias ConfigRequiredCallback = (Payments.ReadConfig) -> Swift.Void
public typealias EMVAppRequiredCallback = (Payments.EmvApp) -> Swift.Void
public typealias RejectRecoverCallback = () -> Swift.Void
public typealias ConfirmCardDataCallback = (Payments.Confirmation) -> Swift.Void
public typealias ConfirmPaymentDataCallback = (Payments.PaymentConfirmation) -> Swift.Void
public typealias ConfirmRefundDataCallback = (Payments.RefundConfirmation) -> Swift.Void
public typealias ConfirmCancelDataCallback = (Payments.CancelConfirmation) -> Swift.Void
public struct EmvApp {
  public var name: Swift.String {
    get
  }
  public var aid: Swift.String? {
    get
  }
  public init(emvApp: TransactionSDK.EmvApp)
}
public enum TransactionIntentError : Swift.Error, Swift.Equatable {
  case invalidDevice
  case connectionFailed
  case readModesNotSupported
  case transactionResult(Payments.TransactionIntentError.TransactionResult)
  case transactionTimeout
  case readError(Payments.TransactionIntentError.ReadError)
  case readerBusy
  case cardInsertion(Payments.TransactionIntentError.CardInsertion)
  case internalError
  case invalidParameter(Swift.String?)
  case configurationError
  case aborted
  case rejected
  public enum ReadError : Swift.Error, Swift.Equatable {
    case swipe(Payments.TransactionIntentError.ReadError.Swipe)
    case chip(Payments.TransactionIntentError.ReadError.Chip)
    case nfc(Payments.TransactionIntentError.ReadError.Nfc)
    public enum Swipe {
      case invalidData
      case badSwipe
      public static func == (a: Payments.TransactionIntentError.ReadError.Swipe, b: Payments.TransactionIntentError.ReadError.Swipe) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
    public enum Chip {
      case invalidData
      case unsupportedEmvApp
      case fallback
      public static func == (a: Payments.TransactionIntentError.ReadError.Chip, b: Payments.TransactionIntentError.ReadError.Chip) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
    public enum Nfc {
      case invalidData
      case unsupportedEmvApp
      public static func == (a: Payments.TransactionIntentError.ReadError.Nfc, b: Payments.TransactionIntentError.ReadError.Nfc) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
    public static func == (a: Payments.TransactionIntentError.ReadError, b: Payments.TransactionIntentError.ReadError) -> Swift.Bool
  }
  public enum TransactionResult : Swift.Equatable {
    case declined(message: Swift.String?, reason: Payments.TransactionIntentError.TransactionResult.Reason)
    case terminated
    public enum Reason : Swift.Equatable {
      case onlineDeclined(code: Swift.String?, processorCode: Swift.String?)
      case offlineDeclined
      case invalidData
      case serverError
      case timeout
      case unauthorized
      case tryAgain
      case hardwareCancelled
      public static func == (a: Payments.TransactionIntentError.TransactionResult.Reason, b: Payments.TransactionIntentError.TransactionResult.Reason) -> Swift.Bool
    }
    public static func == (a: Payments.TransactionIntentError.TransactionResult, b: Payments.TransactionIntentError.TransactionResult) -> Swift.Bool
  }
  public enum CardInsertion {
    case mustRemoveCard
    case cardMustBeInserted
    public static func == (a: Payments.TransactionIntentError.CardInsertion, b: Payments.TransactionIntentError.CardInsertion) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public static func == (a: Payments.TransactionIntentError, b: Payments.TransactionIntentError) -> Swift.Bool
}
extension Payments.TransactionIntentError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
public enum TransactionIntentInitializationError : Swift.Error {
  case missingInitialization(Swift.String)
  case unavailableAbort(Swift.String)
  case unknown
}
public protocol SwipePinDecider {
  func decide(card: Payments.TransactionCard) -> Swift.Bool
}
@_hasMissingDesignatedInitializers final public class ReaderConfigurator {
  final public func abort() throws
  @objc deinit
}
extension Payments.ReaderConfigurator : TransactionSDK.ReaderStatusDelegate {
  final public func onStatusChanged(status: TransactionSDK.ReaderStatus, for device: (any TransactionSDK.Device)?, readerInfo: TransactionSDK.ReaderInfo?)
}
public typealias TransactionIntentResult = Swift.Result<Payments.TransactionIntent, Payments.TransactionIntentInitializationError>
@_hasMissingDesignatedInitializers final public class TransactionIntentFactory {
  public static func createTransactionIntent(type: Payments.TransactionType, listener: any Payments.SDKTransactionListener, gateway: (any TransactionSDK.CancelTransactionHandler & TransactionSDK.RefundTransactionHandler & TransactionSDK.ReversalTransactionHandler & TransactionSDK.SaleTransactionHandler)? = nil, completion: @escaping (Payments.TransactionIntentResult) -> Swift.Void)
  @objc deinit
}
public enum TransactionType {
  case payment
  case refund
  case cancel
  public static func == (a: Payments.TransactionType, b: Payments.TransactionType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension Payments.DeviceType : Swift.Equatable {}
extension Payments.DeviceType : Swift.Hashable {}
extension Payments.ReaderType : Swift.Equatable {}
extension Payments.ReaderType : Swift.Hashable {}
extension Payments.DeviceResultErrorType : Swift.Equatable {}
extension Payments.DeviceResultErrorType : Swift.Hashable {}
extension Payments.CvmResult : Swift.Equatable {}
extension Payments.CvmResult : Swift.Hashable {}
extension Payments.EstablishmentType : Swift.Equatable {}
extension Payments.EstablishmentType : Swift.Hashable {}
extension Payments.EstablishmentType : Swift.RawRepresentable {}
extension Payments.Exponent : Swift.Equatable {}
extension Payments.Exponent : Swift.Hashable {}
extension Payments.CardType : Swift.Equatable {}
extension Payments.CardType : Swift.Hashable {}
extension Payments.CardType : Swift.Sendable {}
extension Payments.AccountType : Swift.Equatable {}
extension Payments.AccountType : Swift.Hashable {}
extension Payments.LogLevel : Swift.Equatable {}
extension Payments.LogLevel : Swift.Hashable {}
extension Payments.PaymentsConfigurationError : Swift.Equatable {}
extension Payments.PaymentsConfigurationError : Swift.Hashable {}
extension Payments.TransactionIntentError.ReadError.Swipe : Swift.Equatable {}
extension Payments.TransactionIntentError.ReadError.Swipe : Swift.Hashable {}
extension Payments.TransactionIntentError.ReadError.Chip : Swift.Equatable {}
extension Payments.TransactionIntentError.ReadError.Chip : Swift.Hashable {}
extension Payments.TransactionIntentError.ReadError.Nfc : Swift.Equatable {}
extension Payments.TransactionIntentError.ReadError.Nfc : Swift.Hashable {}
extension Payments.TransactionIntentError.CardInsertion : Swift.Equatable {}
extension Payments.TransactionIntentError.CardInsertion : Swift.Hashable {}
extension Payments.TransactionType : Swift.Equatable {}
extension Payments.TransactionType : Swift.Hashable {}
