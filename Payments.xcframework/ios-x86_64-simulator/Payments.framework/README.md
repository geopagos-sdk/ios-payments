# Local Installation

### Pre requisites
- Use xcode 14.2
- execute: `gem install bundler -v 2.3.26` Installs Bundler version 2.3.26 in your local machine 
- install: https://github.com/RobotsAndPencils/xcodes#installation Install ```xcodes``` this tools must be installed in order to run the test lane

### Setup
- clone repo
- access repo's folder from the console
- execute: `bundle install` (to install all ruby gems specified in the Gemfile)
- execute: `bundle exec pod install` (to download and install all external dependencies)

# Deploy instructions

https://geopagos.atlassian.net/wiki/spaces/MOB/pages/2968911895/Deploy
