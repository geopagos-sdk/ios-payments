# Installation

### Pre requisites
- Stack xcode 15.4
- Minimum deployment target: iOS 13.0
- install: https://github.com/RobotsAndPencils/xcodes#installation Install ```xcodes``` this tools must be installed in order to run the test lane

### Setup
- clone repo
- access repo's folder from the console
- execute: `sh install_gems.sh` Installs Bundler version 2.4.22 in your local machine 
- execute: `sh install_pods.sh` Installs Pods


# Deploy instructions

https://geopagos.atlassian.net/wiki/spaces/MOB/pages/2968911895/Deploy
