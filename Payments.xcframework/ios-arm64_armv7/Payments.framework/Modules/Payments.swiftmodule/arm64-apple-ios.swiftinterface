// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6.1 (swiftlang-5.6.0.323.66 clang-1316.0.20.12)
// swift-module-flags: -target arm64-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Payments
import Foundation
@_exported import Payments
import QPosHardware
import Swift
import TransactionSDK
import _Concurrency
public typealias ConfigurationRequiredCallback = (TransactionSDK.ReaderConfigurationFiles) -> Swift.Void
public protocol ReaderConfiguratorDelegate : AnyObject {
  func onReaderStateChanged(state: Payments.ReaderState)
  func onDeviceRequired(provideDevice: @escaping Payments.DeviceRequiredCallback)
  func onConfigurationRequired(configuration: @escaping Payments.ConfigurationRequiredCallback)
  func onConfigurationCompleted()
  func onRecoverableError(error: Payments.ReaderConfiguratorError, recover: @escaping Payments.RejectRecoverCallback)
  func onNonRecoverableError(error: Payments.ReaderConfiguratorError, message: Swift.String?)
}
final public class DeviceConfigurationBuilder {
  public init()
  final public func setTerminalCapabilities(_ value: Swift.String) -> Payments.DeviceConfigurationBuilder
  final public func setCountryCode(_ value: Swift.String) -> Payments.DeviceConfigurationBuilder
  final public func setContactCvmLimit(_ value: Payments.Money) -> Payments.DeviceConfigurationBuilder
  final public func setContactlessCvmLimit(_ value: Payments.Money) -> Payments.DeviceConfigurationBuilder
  final public func build() -> TransactionSDK.DeviceConfiguration
  @objc deinit
}
public protocol Device {
  var device: TransactionSDK.Device { get }
  var id: ObjectiveC.NSObject { get }
  var name: Swift.String { get }
  var type: Payments.DeviceType { get }
  var strength: Payments.DeviceConnectionStrength { get }
}
public struct NormalDevice : Payments.Device {
  public let device: TransactionSDK.Device
  public var id: ObjectiveC.NSObject {
    get
  }
  public var name: Swift.String {
    get
  }
  public var type: Payments.DeviceType {
    get
  }
  public var strength: Payments.DeviceConnectionStrength {
    get
  }
}
public struct QposMiniFirmwareRestoreDevice : Payments.Device {
  public var device: TransactionSDK.Device {
    get
  }
  public var id: ObjectiveC.NSObject {
    get
  }
  public var name: Swift.String {
    get
  }
  public var type: Payments.DeviceType {
    get
  }
  public var strength: Payments.DeviceConnectionStrength {
    get
  }
  public init(device: Payments.Device, readerInfo: Payments.ReaderInfo)
}
public enum DeviceType {
  case qpos
  case magicPos
  case unknown
  public static func == (a: Payments.DeviceType, b: Payments.DeviceType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers final public class DeviceConnectionStrength : Swift.Comparable {
  public static func < (lhs: Payments.DeviceConnectionStrength, rhs: Payments.DeviceConnectionStrength) -> Swift.Bool
  public static func == (lhs: Payments.DeviceConnectionStrength, rhs: Payments.DeviceConnectionStrength) -> Swift.Bool
  @objc deinit
}
public struct ReaderInfo {
  public let id: Swift.String
  public let serial: Swift.String
  public let firmware: Swift.String?
  public let hardwareVersion: Swift.String?
  public let readerType: Payments.ReaderType
  public init(id: Swift.String, serial: Swift.String, firmware: Swift.String?, hardwareVersion: Swift.String?, readerType: Payments.ReaderType)
}
public enum ReaderType {
  case qposMini
  case qposCute
  case qposBt
  case magicPos
  public static func == (a: Payments.ReaderType, b: Payments.ReaderType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias DeviceResultCallback = (Payments.DeviceResult) -> Swift.Void
final public class DeviceScanService {
  public init(deviceType: Payments.DeviceType)
  public init()
  final public func scan(timeout: Foundation.TimeInterval = 10.0, callback: @escaping Payments.DeviceResultCallback)
  final public func stopScanning()
  @objc deinit
}
@frozen public enum DeviceResult {
  case success(list: [Payments.Device], isScanning: Swift.Bool)
  case error(type: Payments.DeviceResultErrorType, isScanning: Swift.Bool)
}
public enum DeviceResultErrorType {
  case permissionError
  case nothingFound
  case hardwareError
  public static func == (a: Payments.DeviceResultErrorType, b: Payments.DeviceResultErrorType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct PaymentsSDK {
  public static func configure(paymentsConfiguration: Payments.PaymentsConfiguration)
}
public struct ReadConfig {
  public let readModes: Swift.Set<TransactionSDK.CardReadMode>
  public let timeout: Foundation.TimeInterval
  public let cardInsertionStatus: TransactionSDK.CardInsertionStatus
  public let transactionTotal: Payments.TransactionTotal
  public init(readModes: Swift.Set<TransactionSDK.CardReadMode>, timeout: Foundation.TimeInterval, cardInsertionStatus: TransactionSDK.CardInsertionStatus = .cardNotInserted, transactionTotal: Payments.TransactionTotal)
}
public enum ReaderState {
  case notConnected
  case connecting(Payments.Device)
  case connected(Payments.Device, TransactionSDK.ReaderInfo, Payments.ReaderState.Connected)
  public enum Connected {
    case idle
    case waitingForCard
    case processing(TransactionSDK.CardReadMode)
    case emvRequestingPin
  }
}
public enum TransactionCard {
  case magnetic(Payments.MagneticCard)
  case emv(Payments.EmvCard)
}
@_hasMissingDesignatedInitializers public class Card {
  final public let maskedPan: Swift.String
  final public let cardHolderName: Swift.String?
  final public let readMode: TransactionSDK.CardReadMode
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class MagneticCard : Payments.Card {
  final public let serviceCode: Swift.String?
  final public let needPin: Swift.Bool
  final public let hasChip: Swift.Bool
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class EmvCard : Payments.Card {
  final public let emvApp: Payments.EmvApp?
  final public let cvmResult: Payments.CvmResult
  @objc deinit
}
public enum CvmResult {
  case noCvm, signature, pin
  public static func == (a: Payments.CvmResult, b: Payments.CvmResult) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers final public class ReaderConfiguratorFactory {
  public static func createConfigurator(delegate: Payments.ReaderConfiguratorDelegate) throws -> Payments.ReaderConfigurator
  @objc deinit
}
public enum ReaderConfiguratorError {
  case invalidDevice
  case connectionFailed
  case invalidConfiguration(Swift.String?)
  case configurationError(Swift.String?)
  case internalError
  case aborted
}
public enum ReaderConfiguratorInitializationError : Swift.Error {
  case missingInitialization(Swift.String)
}
public enum CannotAbortException : Swift.Error {
  case cannotAbort(message: Swift.String)
}
@_hasMissingDesignatedInitializers final public class TransactionIntent {
  final public func abort() throws
  @objc deinit
}
extension Payments.TransactionIntent : TransactionSDK.ReaderStatusDelegate {
  final public func onStatusChanged(status: TransactionSDK.ReaderStatus)
}
extension Payments.TransactionIntent : TransactionSDK.SwipePinDecider {
  final public func pinRequired(card: TransactionSDK.TransactionCard) -> Swift.Bool
}
@_hasMissingDesignatedInitializers final public class DeviceConfigurator {
  public static func deviceWithConfiguration(device: Payments.Device, deviceConfiguration: TransactionSDK.DeviceConfiguration) throws -> Payments.Device
  @objc deinit
}
public enum DeviceConfiguratorError : Swift.Error {
  case deviceNotAllow(Swift.String?)
}
public struct Confirmation {
  public init(authToken: Swift.String, isFallback: Swift.Bool, terminalId: Swift.String, merchant: Payments.Merchant, establishment: Payments.Establishment, additionalCardData: Payments.AdditionalCardData, refNumber: Swift.String, timeZone: Foundation.TimeZone, ticketNumber: Swift.String? = nil, accountType: Payments.AccountType? = nil, externalData: Swift.String? = nil)
}
public struct PaymentConfirmation {
  public init(confirmation: Payments.Confirmation, installments: Swift.Int?, paymentPlan: Swift.String?, updatedAmount: Payments.TransactionTotal? = nil)
}
public struct RefundConfirmation {
  public init(confirmation: Payments.Confirmation, parentRefNumber: Swift.String)
}
public struct CancelConfirmation {
  public init(confirmation: Payments.Confirmation, parentRefNumber: Swift.String)
}
public enum TransactionApproved {
  case sale(Payments.TransactionCard, Payments.PaymentConfirmation, Payments.PaymentData)
  case refund(Payments.TransactionCard, Payments.RefundConfirmation, Payments.PaymentData)
  case cancel(Payments.TransactionCard, Payments.CancelConfirmation, Payments.PaymentData)
}
public struct PaymentData {
  public let authCode: Swift.String?
}
public struct Merchant {
  public init(merchantId: Swift.String, merchantName: Swift.String? = nil)
}
public struct AdditionalCardData {
  public init(cvv: Swift.String?, brand: Swift.String, cardType: Payments.CardType)
}
public class Establishment {
  public init(address: Payments.Address? = nil, businessCategoryCode: Swift.String? = nil, docNumber: Swift.String? = nil, docType: Swift.String? = nil, email: Swift.String? = nil, legalName: Swift.String, phoneNumber: Swift.String? = nil, tradeName: Swift.String? = nil, establishmentType: Payments.EstablishmentType, intermediary: Payments.Establishment? = nil)
  @objc deinit
}
public struct Address {
  public init(addressLine1: Swift.String? = nil, addressLine2: Swift.String? = nil, city: Swift.String, countryCode: Swift.String, postalCode: Swift.String, state: Swift.String)
}
public enum EstablishmentType : Swift.String {
  case endMerchant
  case intermediary
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct Money {
  public init(amount: Foundation.Decimal, currency: Payments.Currency) throws
}
public enum MoneyError : Swift.Error {
  case moneyInvalid(Swift.String)
}
public struct Currency {
  public static let ars: Payments.Currency
  public static let usd: Payments.Currency
  public static let mxn: Payments.Currency
  public static let cop: Payments.Currency
  public init(numericCode: Swift.String, alphabeticCode: Swift.String, exponent: Payments.Exponent = .two)
}
extension Payments.Currency : Swift.Equatable {
  public static func == (left: Payments.Currency, right: Payments.Currency) -> Swift.Bool
}
public enum Exponent {
  case zero
  case two
  public static func == (a: Payments.Exponent, b: Payments.Exponent) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Tax {
  public init(amount: Payments.Money, label: Swift.String)
}
public protocol Taxes {
  var total: Payments.Money? { get }
  var currency: Payments.Currency? { get }
}
public struct BreakDownTaxes : Payments.Taxes {
  public var total: Payments.Money? {
    get
  }
  public var currency: Payments.Currency? {
    get
  }
  public init(taxes: [Payments.Tax]) throws
}
public struct TotalTaxes : Payments.Taxes {
  public var total: Payments.Money? {
    get
  }
  public var currency: Payments.Currency? {
    get
  }
  public init(taxes: Payments.Money)
}
public struct TransactionTotal {
  public var total: Payments.Money {
    get
  }
  public init(net: Payments.Money, taxes: Payments.Taxes) throws
  public init(gross: Payments.Money)
}
extension TransactionSDK.Exponent {
  public init(exponent: Payments.Exponent)
}
@frozen public enum CardType {
  case credit
  case debit
  public static func == (a: Payments.CardType, b: Payments.CardType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum AccountType {
  case savingAccount
  case transactionAccount
  public static func == (a: Payments.AccountType, b: Payments.AccountType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol SDKLogger {
  func log(message: Swift.String, level: Payments.LogLevel)
}
public enum LogLevel {
  case debug, info, notice, error, fault
  public static func == (a: Payments.LogLevel, b: Payments.LogLevel) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers final public class PaymentsConfiguration {
  public class Builder {
    public init(endpoint: Swift.String, applicationKey: Swift.String, readerInstallers: [TransactionSDK.ReaderInstaller])
    public func setSwipePinDecider(swipePinDecider: Payments.SwipePinDecider) -> Payments.PaymentsConfiguration.Builder
    public func setLogger(logger: Payments.SDKLogger) -> Payments.PaymentsConfiguration.Builder
    public func build() -> Payments.PaymentsConfiguration
    @objc deinit
  }
  @objc deinit
}
public protocol SwipePinDecider {
  func decide(card: Payments.TransactionCard) -> Swift.Bool
}
public typealias DeviceRequiredCallback = (Payments.Device) -> Swift.Void
public typealias ConfigRequiredCallback = (Payments.ReadConfig) -> Swift.Void
public typealias EMVAppRequiredCallback = (Payments.EmvApp) -> Swift.Void
public typealias RejectRecoverCallback = () -> Swift.Void
public typealias ConfirmCardDataCallback = (Payments.Confirmation) -> Swift.Void
public typealias ConfirmPaymentDataCallback = (Payments.PaymentConfirmation) -> Swift.Void
public typealias ConfirmRefundDataCallback = (Payments.RefundConfirmation) -> Swift.Void
public typealias ConfirmCancelDataCallback = (Payments.CancelConfirmation) -> Swift.Void
public protocol TransactionIntentDelegate : AnyObject {
  func onReaderStateChanged(state: Payments.ReaderState)
  func onDeviceRequired(provideDevice: @escaping Payments.DeviceRequiredCallback)
  func onReadConfigRequired(provideReadConfig: @escaping Payments.ConfigRequiredCallback)
  func onSelectEmvAppRequired(availableAIDs: [Payments.EmvApp], selectEmvApp: @escaping Payments.EMVAppRequiredCallback)
  func onConfirmPaymentData(card: Payments.TransactionCard, accept: @escaping Payments.ConfirmPaymentDataCallback, reject: @escaping Payments.RejectRecoverCallback)
  func onConfirmRefundData(card: Payments.TransactionCard, accept: @escaping Payments.ConfirmRefundDataCallback, reject: @escaping Payments.RejectRecoverCallback)
  func onConfirmCancelData(card: Payments.TransactionCard, accept: @escaping Payments.ConfirmCancelDataCallback, reject: @escaping Payments.RejectRecoverCallback)
  func onTransactionApproved(transactionApproved: Payments.TransactionApproved)
  func onRecoverableError(error: Payments.TransactionIntentError, recover: @escaping Payments.RejectRecoverCallback)
  func onNonRecoverableError(error: Payments.TransactionIntentError, message: Swift.String?)
}
public struct EmvApp {
  public var name: Swift.String {
    get
  }
  public var aid: Swift.String? {
    get
  }
}
public enum TransactionIntentError {
  case invalidDevice
  case connectionFailed
  case readModesNotSupported
  case transactionResult(Payments.TransactionIntentError.TransactionResult)
  case transactionTimeout
  case readError(Payments.TransactionIntentError.ReadError)
  case readerBusy
  case cardInsertion(Payments.TransactionIntentError.CardInsertion)
  case internalError
  case invalidParameter(Swift.String?)
  case configurationError
  case aborted
  case rejected
  public enum ReadError {
    case swipe(Payments.TransactionIntentError.ReadError.Swipe)
    case chip(Payments.TransactionIntentError.ReadError.Chip)
    case nfc(Payments.TransactionIntentError.ReadError.Nfc)
    public enum Swipe {
      case invalidData
      case badSwipe
      public static func == (a: Payments.TransactionIntentError.ReadError.Swipe, b: Payments.TransactionIntentError.ReadError.Swipe) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
    public enum Chip {
      case invalidData
      case unsupportedEmvApp
      case fallback
      public static func == (a: Payments.TransactionIntentError.ReadError.Chip, b: Payments.TransactionIntentError.ReadError.Chip) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
    public enum Nfc {
      case invalidData
      case unsupportedEmvApp
      public static func == (a: Payments.TransactionIntentError.ReadError.Nfc, b: Payments.TransactionIntentError.ReadError.Nfc) -> Swift.Bool
      public func hash(into hasher: inout Swift.Hasher)
      public var hashValue: Swift.Int {
        get
      }
    }
  }
  public enum TransactionResult {
    case declined(message: Swift.String?, reason: Payments.TransactionIntentError.TransactionResult.Reason)
    case terminated
    public enum Reason {
      case onlineDeclined(code: Swift.String?, processorCode: Swift.String?)
      case offlineDeclined
      case invalidData
      case serverError
      case timeout
      case unauthorized
      case tryAgain
    }
  }
  public enum CardInsertion {
    case mustRemoveCard
    case cardMustBeInserted
    public static func == (a: Payments.TransactionIntentError.CardInsertion, b: Payments.TransactionIntentError.CardInsertion) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
}
public enum TransactionIntentInitializationError : Swift.Error {
  case missingInitialization(Swift.String)
}
@_hasMissingDesignatedInitializers final public class ReaderConfigurator {
  final public func abort() throws
  @objc deinit
}
extension Payments.ReaderConfigurator : TransactionSDK.ReaderStatusDelegate {
  final public func onStatusChanged(status: TransactionSDK.ReaderStatus)
}
@_hasMissingDesignatedInitializers final public class TransactionIntentFactory {
  public static func createTransactionIntent(type: Payments.TransactionType, delegate: Payments.TransactionIntentDelegate) throws -> Payments.TransactionIntent
  @objc deinit
}
public enum TransactionType {
  case payment
  case refund
  case cancel
  public static func == (a: Payments.TransactionType, b: Payments.TransactionType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension Payments.DeviceType : Swift.Equatable {}
extension Payments.DeviceType : Swift.Hashable {}
extension Payments.ReaderType : Swift.Equatable {}
extension Payments.ReaderType : Swift.Hashable {}
extension Payments.DeviceResultErrorType : Swift.Equatable {}
extension Payments.DeviceResultErrorType : Swift.Hashable {}
extension Payments.CvmResult : Swift.Equatable {}
extension Payments.CvmResult : Swift.Hashable {}
extension Payments.EstablishmentType : Swift.Equatable {}
extension Payments.EstablishmentType : Swift.Hashable {}
extension Payments.EstablishmentType : Swift.RawRepresentable {}
extension Payments.Exponent : Swift.Equatable {}
extension Payments.Exponent : Swift.Hashable {}
extension Payments.CardType : Swift.Equatable {}
extension Payments.CardType : Swift.Hashable {}
extension Payments.CardType : Swift.Sendable {}
extension Payments.AccountType : Swift.Equatable {}
extension Payments.AccountType : Swift.Hashable {}
extension Payments.LogLevel : Swift.Equatable {}
extension Payments.LogLevel : Swift.Hashable {}
extension Payments.TransactionIntentError.ReadError.Swipe : Swift.Equatable {}
extension Payments.TransactionIntentError.ReadError.Swipe : Swift.Hashable {}
extension Payments.TransactionIntentError.ReadError.Chip : Swift.Equatable {}
extension Payments.TransactionIntentError.ReadError.Chip : Swift.Hashable {}
extension Payments.TransactionIntentError.ReadError.Nfc : Swift.Equatable {}
extension Payments.TransactionIntentError.ReadError.Nfc : Swift.Hashable {}
extension Payments.TransactionIntentError.CardInsertion : Swift.Equatable {}
extension Payments.TransactionIntentError.CardInsertion : Swift.Hashable {}
extension Payments.TransactionType : Swift.Equatable {}
extension Payments.TransactionType : Swift.Hashable {}
