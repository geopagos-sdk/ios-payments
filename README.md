# Payments iOS SDK

## Installation

### CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For usage and installation instructions, visit their website.
To integrate the SDK into your Xcode project using CocoaPods, first add the souces to the top of your  `Podfile`:

```ruby
source 'https://bitbucket.org/geopagos-sdk/ios-specs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

Then add the pod to your target:

```ruby
  pod 'Payments', '15.0.2'
```

If you want to use magicpos reader then you need to add its pod

```ruby
  pod 'MagicPosHardware', '9.2.7'
```

If you want to use qpos reader then you need to add its pod

```ruby
  pod 'QPosHardware', '9.2.7'
```

Example full  `Podfile`, if your target is named `MyTarget`:

```ruby
source 'https://bitbucket.org/geopagos-sdk/ios-specs.git'
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '13.0'

target 'MyTarget' do
  use_frameworks!

  # Pods for MyTarget
  pod 'Payments', '15.0.2'
  pod 'MagicPosHardware', '9.2.7'
  pod 'QPosHardware', '9.2.7'
  
end
```

## Reference

For class and method referenes, checkout this repository and open `docs/index.html`

## Usage

- Import the SDK

```swift
import Payments
```

- Create a PaymentsConfiguration for configure the endpoint, application key and readerInstallers (optionally a logger object and SwipePinDecider implementation)
- Call PaymentsSDK.configure(...) for configure the SDK

```swift

    let paymentsConfiguration = PaymentsConfiguration.Builder(endpoint: "https://www.example.com", applicationKey: TOKEN, readerInstallers: [<QposReaderInstaller>, <MagicPosReaderInstaller>])
            .setLogger(logger: customLogger)
            .setSwipePinDecider(swipePinDecider: customtSwipePinDecider)
            .build()
            
    PaymentsSDK.configure(paymentsConfiguration: paymentsConfiguration)
```

Note: To create readerInstallers is necessary import the module corresponding to each Reader

For Magic

```swift
import MagicPosHardware 
    
    let magicPosReaderInstaller = MagicPosReaderInstaller()
```

For Qpos

```swift
import QPosHardware
    
    let qposReaderInstaller = QposReaderInstaller()
```

- Scan for devices
For make a transaction is necessary to use a card reader, in order to scan devices(that represents card readers) can be use the class DeviceScanService 

```swift
let deviceService = DeviceScanService()

deviceService.scan { (result: DeviceResult) in
    switch result {
    case .success(let list, let isScanning):
        for device: Device in list {
            //...
        }
    case .error(let type, let isScanning):
        //handle errors
    }
}

```

for stop the scan call

```swift
    deviceService.stopScanning()
```

Once you have a device, a transaction can be created

## Make transaction

- Start a Transaction
For start a transaction create a `TransactionIntent`, use the `TransactionIntentFactory`

```swift
let transactionIntent = TransactionIntentFactory.createTransactionIntent(type: type, delegate: delegate)
```
This method recieve transaction type (.payments, .refund, .cancel) and the class instance that implements to `TransactionIntentDelegate` protocol

once the transactionIntent is create, the methods of `TransactionIntentDelegate` will begin to be called, the `TransactionIntentDelegate` methods guides you through the data that must be provided

Example of `TransactionIntentDelegate` implementation (reference the SDK documentation to see all the possibilities and method descriptions)

- the method `onReaderStateChanged` provide feedback the reader state

```swift
    func onReaderStateChanged(state: ReaderState) {
        switch state {
        case .notConnected:
            print("Not Connected")
        case .connecting(let device):
            print("Connecting... \(device.name)")
        case .connected(let device, let readerInfo, .idle):
            print("Idle: \(device.name) - Battery: \(readerInfo.batteryInfo?.percentage ?? 0)")
        case .connected(let device, let readerInfo, .waitingForCard):
            print("Waiting for Card: \(device.name) - Firmware: \(readerInfo.firmware ?? "")")
        case .connected(let device, let readerInfo, .processing(let mode)):
            print("Processing: \(device.name) - Serial: \(readerInfo.serial) - Mode: \(mode)")
        case .connected(_, _, .emvRequestingPin):
            print("Processing: emvRequestingPin")
        @unknown default:
            break
        }
    }
```

- The method `onDeviceRequired` is called for provide a device for make the connection

```swift
    func onDeviceRequired(provideDevice: @escaping DeviceRequiredCallback) {
        provideDevice(device) // device scan previously
    }
```    

- The method `onReadConfigRequired` is called for provide a read config (read modes, timeout, ect)

```swift
    func onReadConfigRequired(provideReadConfig: @escaping ConfigRequiredCallback) {
        let transactionTotal = TransactionTotal(...)
        let readConfig = ReadConfig(readModes: [.swipe, .chip, .nfc], timeout: 10, transactionTotal: transactionTotal)
        provideReadConfig(readConfig)
    }
```

- The method `onSelectEmvAppRequired` is called if the card have more one emv app

```swift
    func onSelectEmvAppRequired(availableAIDs: [EmvApp], selectEmvApp: @escaping EMVAppRequiredCallback) {
        if let emvApp = availableAIDs.first {
            selectEmvApp(emvApp)
        }
    }
```

- The method `onConfirmPaymentData` is called for provide the sale confirmation data

```swift
    func onConfirmPaymentData(card: TransactionCard, accept: @escaping ConfirmPaymentDataCallback, reject: @escaping RejectRecoverCallback) {
        let confirmation = Confirmation(...)
        let paymentConfirmation = PaymentConfirmation(confirmation: confirmation, installments: nil, paymentPlan: nil)
        accept(paymentConfirmation)
    }
```

- The method `onConfirmRefundData` is called for provide the refund confirmation data

```swift
    func onConfirmRefundData(card: TransactionCard, accept: @escaping ConfirmRefundDataCallback, reject: @escaping RejectRecoverCallback) {
        let confirmation = Confirmation(...)
        let refundConfirmation = RefundConfirmation(confirmation: confirmation, parentRefNumber: parentRefNumber)
        accept(refundConfirmation)
    }
```

- The method `onConfirmCancelData` is called for provide the cancel confirmation data

```swift
    func onConfirmCancelData(card: TransactionCard, accept: @escaping ConfirmCancelDataCallback, reject: @escaping RejectRecoverCallback) {
        let confirmation = Confirmation(...)
        let cancelConfirmation = CancelConfirmation(confirmation: confirmation, parentRefNumber: parentRefNumber)
        accept(cancelConfirmation)
    }
```

- The method `onTransactionApproved` is called for inform the Transaction approved data

```swift
    func onTransactionApproved(transactionApproved: TransactionApproved) {
        switch transactionApproved {
        case .sale(_, _, let paymentData):
            showTransactionStep("onTransactionApproved \(paymentData.authCode ?? "")")
        case .refund(_, _, let paymentData):
            showTransactionStep("onTransactionApproved \(paymentData.authCode ?? "")")
        case .cancel(_, _, let paymentData):
            showTransactionStep("onTransactionApproved \(paymentData.authCode ?? "")")
        @unknown default:
            break
        }
    }
```

- The method `onRecoverableError` is called if occurs a recoverable error

```swift
    func onRecoverableError(error: TransactionIntentError, recover: @escaping RejectRecoverCallback) {
        switch error {
        ...
        }
        
        //call recover() if you want try recover an error
    }
```

- The method `onNonRecoverableError` is called if occurs a nonrecoverable error

```swift
    func onNonRecoverableError(error: TransactionIntentError, message: String?) {
        print(message)
    }
```


## Reader Update

Reader configuration can be updated using the appropriate configuration files provided by Geopagos team.

To create a `ReaderConfigurator`, use the `ReaderConfiguratorFactory`

```swift
let readerConfigurator = ReaderConfiguratorFactory.createConfigurator(delegate: delegate)
```
Where the delegate implements the `ReaderConfiguratorDelegate` protocol.
Each delegate callback will guide you into completing the configuration, and asking for needed data.

On the `onConfigurationRequired` method, you must provide a `ReaderConfigurationFiles` objects, exists several factories according the configuration type and the reader used, firmware configuration can be done as follows:

for Qpos 
```swift
QposReaderConfigurationFactory.qposFirmwareFiles(firmware: url)
```
where firmware contain the bundle file provided from geopagos

for magicpos 

```swift
MagicPosReaderConfigurationFactory.qposFirmwareFiles(firmware: url)
```
firmware same case that qpos

In the case that the bundle file does not contain a configuration that matching with the configuration type and the reader requested, it will be returned a `.invalidConfiguration` error.

### Qpos mini Firmware update (Edge case)
In case Firmware update failed and reader passes to boot state (non responsive to any pressed key) it is necessary to use a special device to finish the update, to do so we need to add a special mapper first

```swift 
readerConfigurator = try? ReaderConfiguratorFactory.createConfigurator(delegate: self, additionalMappers:[QposMiniFirmwareRestoreDeviceMapper()])
```
and create a ```QposMiniFirmwareRestoreDevice```

```swift
// Example:
 let readerInfo = ReaderInfo(id: "", serial: "", batteryInfo: BatteryInfo(percentage: 1.0, charging: false), firmware: "", hardwareVersion: "A27C_P1", readerType: .qposMini)
let qposMiniFirmwareRestoreDevice = QposMiniFirmwareRestoreDevice(device: device, readerInfo: readerInfo)
```
then the device should be passed in `ReaderConfiguratorDelegate.onDeviceRequired(provideDevice:)`


#### Keeping the Reader charging
Some configuration updates result in a lengthy operation (specially firmware updates) that can take between 4 and 10 minutes to complete. Because of this, it's recommended to ask the user to keep the reader connected to a power source before start updating to avoid a disconnection in the middle of the update due to low battery charge.
In the qpos reader is mandatory connect it to power source when firmware updates is made

## Configure Pin decider
In the case of magnets strip transactions is possible provide custom logic for pin request, exists an protocol SwipePinDecider that may be implemented and provide

```swift
struct CustomtSwipePinDecider: SwipePinDecider {

    func decide(card: TransactionCard) -> Bool {
        //...
    }
}
```
the decide method receive the card data and return a boolean, this custom implementation may be provide when configuration is create

```swift

    let paymentsConfiguration = PaymentsConfiguration.Builder(endpoint: "https://www.example.com", applicationKey: TOKEN, readerInstallers: [<QposReaderInstaller>, <MagicPosReaderInstaller>])
            .setSwipePinDecider(swipePinDecider: customtSwipePinDecider)
            .build()
            
    PaymentsSDK.configure(paymentsConfiguration: paymentsConfiguration)
```

## Configure logger section
The Readers SDK provide an mechanism for collects logs, exists an protocol SDKLogger that may be implemented and provide

```swift
struct CustomSDKLogger: SDKLogger {

    func log(message: String, level: LogLevel) {
        //...
    }
}
```
the log method receive the log message and log level(the logs can by printed for console, save in a file, etc), this custom implementation may be provide when configuration is create

```swift

    let paymentsConfiguration = PaymentsConfiguration.Builder(endpoint: "https://www.example.com", applicationKey: TOKEN, readerInstallers: [<QposReaderInstaller>, <MagicPosReaderInstaller>])
            .setLogger(logger: customLogger)
            .build()
            
    PaymentsSDK.configure(paymentsConfiguration: paymentsConfiguration)
```
